## Co-Expression Network Statistics
+ The list of hubs (based upon 1% criterion and their degrees)

Hub Gene  | Average Degree
------------- | -------------
RPL32  | 9
RPL8A  | 8
RHR2   | 10
RPS3   | 7
RPL5   | 10

+ Average degree of the co-expression network: 1.16

+ Initial alpha: 3.80

+ Final alpha: 4.37

+ Average alpha: 3.99

+ The hub nodes that overlap with BioGRID database: RPL8A, RPS3 

+ The edges that overlap with BioGRID database:

from | to
------------- | -------------
SSA1 | SSA3
COR1 | QCR2
RPL32 | RPP2B
RPL32 | RPL26B
ATP1 | ATP2
RPS11B | RPL30
RPS6B | RPS3
RPS9B | RPL11B
RPL31A | RPL31B
RPL13A | RPL13B
LYS21 | LYS20
RPS11A | RPL30
TPS2 | TSL1
BMH2 | BMH1
EFT2 | EFT1
RPS17B | RPL8A
RPS18A | RPS18B
RPL37B | RPL37A
SMT3 | PDC1
RPS24A | RPS24B
RPL2A | RPL2B
RPL30 | RPL38
RPL24A | RPL8A
RPL28 | RPS5
RPL1B | RPL1A
RPL9A | RPL9B
RPS25A | RPS25B
RPL24B | RPL8A
RPL14B | RPS1A
RPL16A | RPL8B
RPL16A | RPS1A
RPL16A | RPS7B
RPL40A | RPL40B
KAR2 | TMA19
TIF2 | TIF1
RPL17B | RPL17A
RPS5 | RPS6A
RPS4A | RPL5
SSA2 | STI1
RPL8B | RPS1A
RPL8B | RPS1B
RPL15A | RPL15B
RPL10 | RPL13B
RPL10 | RPS7B
RPL10 | RPL25
RPL38 | RPL25
RPS1A | RPL20B
TUB1 | TUB3
RPL20A | RPS3
IDH1 | IDH2
RPS3 | RPS6A
RPL18B | RPL5
RPL18A | RPL5

