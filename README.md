## Co-Expression Network using SPACE method.

The codes are in codes directory. Please install R package _knitr_ using command 

```r
install.packages("knitr")
``` 

The required packages to execute the codes are:

+ igraph
+ space

Note, the code will automatically detect the installation of these two packages and if not installed will install the packages. Next, compile the `co_expression_network.Rmd` using RStudio IDE or through command line:

```r
setwd("path to sparsecorrelation directory") # Set the working directory
library(knitr)
knit2html("./codes/co_expression_network.Rmd")
```

## Folder Description

+ The folder _circos_files_ contains all the required files to generate the circos  image.
+ The folder _codes/utilitycodes_ contains all the helper codes, which was used to generate the co-expression network.
+ The _docs_ folder contains the file [conetworkstats.md](docs/conetworkstats.md) having summary statistics of the co-expression network. 