# Co-Expression Network
Parimal Samir, Rahul, James C. Slaughter, and Andrew Link  
Monday, June 29, 2015  
Load the libraries and data.

```r
packages <- c("space", "igraph")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))  
}
library(space)
library(igraph)
load("../data/data.Rda")
```

```r
hub_nodes<-function(graph,fraction=0.1,highest=TRUE,scores="degree",indices_only=FALSE){
  library(igraph)
  if (scores=="degree"){
    score.degree<-degree(graph)
  }
  indices<-sort(score.degree,index.return=T)  
  n<-round(vcount(graph)*fraction,0)
  
  if( highest==TRUE){
    indices=tail(indices$ix,n)    
  }
  else {
    indices=head(indices$ix,n)
  }
  if (indices_only==TRUE){
    return(indices)
  }
  return(induced.subgraph(graph,indices,impl="auto"))
}
```



```r
#################### Initializing parameters for partial correlation matrix
num.row<-nrow(normalized.matrix)
num.col<-ncol(normalized.matrix)
alpha<-0.1
l1penality<-(1/sqrt(num.row)*qnorm(1-alpha/(2*num.col^2)))*num.row
iter=3
```


```r
  coexp<-space.joint(normalized.matrix, lam1=l1penality, lam2=0.0, weight=2, iter=iter)
  fit.adj<-abs(coexp$ParCor)>0.002
  plot.adj<-fit.adj
  diag(plot.adj)=0
  conetwork<-graph.adjacency(adjmatrix=plot.adj, mode="undirected")
  V(conetwork)$name<-as.character(gene.name)
  V(conetwork)$geneid<-as.character(gene.id)
  V(conetwork)$label<-as.character(gene.name)  
```


```r
V(conetwork)$color=(degree(conetwork)>5)+3
plot(conetwork, vertex.size=3, vertex.frame.color="white",layout=layout.fruchterman.reingold, vertex.label=NA, edge.color=grey(0.5))
```

![](co_expression_network_files/figure-html/graph_net-1.png) 



```r
  hubgraph<-hub_nodes(conetwork,0.01)  
  out.degrees<-degree(conetwork)
  out.degrees<-out.degrees[which(!(V(conetwork)$name %in% V(hubgraph)$name))]
  in.degrees <- out.degrees[which((V(conetwork)$name %in% V(hubgraph)$name))]
  wilcox.test(in.degrees, out.degrees)
```

```
## 
## 	Wilcoxon rank sum test with continuity correction
## 
## data:  in.degrees and out.degrees
## W = 1723, p-value = 0.04414
## alternative hypothesis: true location shift is not equal to 0
```



```r
# For reproducibility
sessionInfo()
```

```
## R version 3.2.0 (2015-04-16)
## Platform: x86_64-w64-mingw32/x64 (64-bit)
## Running under: Windows 8 x64 (build 9200)
## 
## locale:
## [1] LC_COLLATE=English_Canada.1252  LC_CTYPE=English_Canada.1252   
## [3] LC_MONETARY=English_Canada.1252 LC_NUMERIC=C                   
## [5] LC_TIME=English_Canada.1252    
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
## [1] igraph_0.7.1 space_0.1-1 
## 
## loaded via a namespace (and not attached):
##  [1] magrittr_1.5    formatR_1.2     tools_3.2.0     htmltools_0.2.6
##  [5] yaml_2.1.13     stringi_0.5-2   rmarkdown_0.7   knitr_1.10.5   
##  [9] stringr_1.0.0   digest_0.6.8    evaluate_0.7
```

